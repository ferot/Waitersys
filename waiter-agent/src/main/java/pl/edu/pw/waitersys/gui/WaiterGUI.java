package pl.edu.pw.waitersys.gui;

import jade.tools.introspector.Introspector;
import pl.edu.pw.waitersys.Order;
import pl.edu.pw.waitersys.WaiterAgent;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * GUI związane z pojedynczym kelnerem. Wyświetla aktywną listę jego zamówień
 * i pozwala oznaczyć zamówienie jako wykonane.
 */
public class WaiterGUI extends Frame {
    /**
     * Konstruuje GUI. Okno jest nierozerwalnie związane z podanym agentem.
     *
     * <p>
     * Okno nie jest wyświetlane bezpośrednio po skonstruowaniu. Aby było widoczne,
     * należy jawnie wywołać metodę <pre>setVisible(true)</pre>!.
     * </p>
     *
     * @param owner      Agent będący właścicielem GUI.
     * @param waiterName Nazwa kelnera wyświetlana jako tytuł okna.
     */
    public WaiterGUI(WaiterAgent owner, final String waiterName) {
        super(waiterName);

        this.owner = owner;

        setSize(WIN_WIDTH, WIN_HEIGHT);

        final ScrollPane scrollPane = new ScrollPane(ScrollPane.SCROLLBARS_AS_NEEDED);
        final Panel content = new Panel(new BorderLayout());
        content.add(new JLabel("<html><h1>Moje zamówienia:</h1></html>"), BorderLayout.PAGE_START);
        content.add(ordersPanel, BorderLayout.CENTER);
        ordersPanel.setLayout(new BoxLayout(ordersPanel, BoxLayout.Y_AXIS));
        scrollPane.add(content);
        add(scrollPane);

        addWindowListener(new CloseHandler());
    }

    /**
     * Pobiera (zaktualizowane) informacje o zamówieniach agenta-właściciela i renderuje je.
     */
    public void refresh() {
        ordersPanel.removeAll();
        owner.getOrders().forEach(this::renderOrder);
        revalidate();
        repaint();
    }

    /**
     * Renderuje informacje o podanym zamówieniu.
     *
     * @param order Zamówienie, które powinno zostać wyświetlone.
     */
    private void renderOrder(final Order order) {
        if(order != null) {
            final Panel panel = new Panel(new FlowLayout());


            final JLabel info = new JLabel(order.toHTMLWithTableName());
            panel.add(info);

            final Button markDone = new Button("WYKONANO");
            markDone.addActionListener(new MarkDoneHandler(order));
            panel.add(markDone);

            ordersPanel.add(panel);
        }
    }

    /**
     * Obsługuje zdarzenia związane z oknem aplikacji.
     */
    private class CloseHandler extends WindowAdapter {
        /**
         * Obsługuje prośbę o zamknięcie okna (na przykład po kliknięciu X).
         * Zabija agenta-właściciela, a później samo okno.
         */
        public void windowClosing(WindowEvent event) {
            owner.doDelete();
            dispose();
        }
    }

    /**
     * Obsługuje kliknięcia w przycisk oznaczania danego zamówienia jako wykonane.
     * Usuwa zamówienie z listy zamówień agenta.
     */
    private class MarkDoneHandler implements ActionListener {
        MarkDoneHandler(final Order order) {
            this.handledOrder = order;
        }

        public void actionPerformed(ActionEvent e) {
            owner.markOrderDone(handledOrder.getId());
            refresh();
        }

        private final Order handledOrder;
    }

    /**
     * Właściciel GUI.
     */
    private final WaiterAgent owner;

    /**
     * Panel zawierający zamówienia kelnera.
     */
    private final Panel ordersPanel = new Panel();

    /**
     * Wysokość okna.
     */
    private static final int WIN_WIDTH = 400;

    /**
     * Szerokość okna.
     */
    private static final int WIN_HEIGHT = 600;
}
