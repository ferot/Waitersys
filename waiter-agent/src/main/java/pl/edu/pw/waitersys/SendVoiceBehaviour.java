package pl.edu.pw.waitersys;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.*;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import pl.edu.pw.waitersys.messages.OrderMessage;
import pl.edu.pw.waitersys.messages.WaiterPropositionMessage;

import java.io.IOException;

/**
 * Obliczanie siły głosu - deklaracji o możliwości/chęci przetworzenia zadania
 */
public class SendVoiceBehaviour extends CyclicBehaviour {
    //referencja na agenta adaptującego zachowanie
    WaiterAgent waiter;

    public SendVoiceBehaviour(Agent a) {
        super(a);
        waiter = (WaiterAgent) a;
    }

    public void action() {
        MessageTemplate mT = MessageTemplate.MatchPerformative(ACLMessage.REQUEST);
        ACLMessage msg = myAgent.receive(mT);

        if (msg != null) { //Otrzymana od stolika info o nowym zamówieniu
            try {
                waiter.setCachedOrder((Order) msg.getContentObject());
                //TODO wykorzystać obiekt do obliczania siły głosu ?
                double voiceVolume = waiter.calcVoice();
                informTable(msg, voiceVolume);
            } catch (UnreadableException e) {
                e.printStackTrace();
            }

        } else {
            // uśpij jesli brak wiadomości
            block();
        }
    }


    /**
     * Wysyła do agenta-stolika informację o sile swojego głosu.
     *
     * @param agent  - ID stolika-nadawcy
     * @param volume - siła głosu
     */
    private void informTable(ACLMessage msg, double volume) {
        try {
            WaiterProposition wp = new WaiterProposition(volume, waiter.getLocation());
            WaiterPropositionMessage wmsg = new WaiterPropositionMessage(wp, msg.getSender());
            wmsg.setConversationId(msg.getConversationId()); // niezbędne! (stolik filtruje wiadomości po convers. id)

            myAgent.send(wmsg);
            System.out.println("Agent [" + myAgent.getName() + "] wysłał wartość głosu : " + Double.toString(volume));
        } catch (IOException exc) {
            System.err.println("Nie udało się odpowiedzieć agentowi stolika!");
        }
    }
}
