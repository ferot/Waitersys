package pl.edu.pw.waitersys;

import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.core.AID;
import pl.edu.pw.waitersys.directory.WaiterDirectory;
import jade.lang.acl.MessageTemplate;

import jade.domain.FIPAException;
import java.util.ArrayList;


//public class BehaviourChangeOrderRequest extends SimpleBehaviour {
public class BehaviourChangeOrderRequest extends CyclicBehaviour {
    private class OrderInfo {
        public Order order;
        public AID agent;
    }


    private WaiterAgent myAgent; // Agent na rzecz którego wykonywane jest zachowanie
    private int State = 0; // stan maszyny stanowej
    private ArrayList<OrderInfo> ProposedOrders = new ArrayList<>();
    private Order worstOrder;
    private int numberOfReceivers = 0;

    public BehaviourChangeOrderRequest(WaiterAgent waiterAgent) {
        myAgent = waiterAgent;

    }


    private OrderInfo findBestOrder() {
        /* @TODO implement this */

        OrderInfo bestOrder = new OrderInfo();
        int best_diff = 999999;

        for (OrderInfo o : ProposedOrders) {
            int tmp_diff = Math.abs(myAgent.getLocation() - o.order.getLocation());
            if (tmp_diff < best_diff) {
                best_diff = tmp_diff;
                bestOrder = o;
            }

        }

        ProposedOrders.remove(bestOrder);

        return bestOrder;

        /* remove best order*/
    }

    private Order findBestOrder(ArrayList<Order> list) {
        /* @TODO implement this*/
        Order bestOrder = new Order();
        int best_diff = 999999;

        for (Order o : list) {
            int tmp_diff = Math.abs(myAgent.getLocation() - o.getLocation());
            if (tmp_diff < best_diff) {
                best_diff = tmp_diff;
                bestOrder = o;
            }

        }
        return bestOrder;

    }

    @Override
    public void action() {
        try {

            myAgent.locationActualization();
            switch (State) {
                case (0):
                    if (myAgent.getOrderCount() < 3) {
                        State = 0;
                        return;
                    }
                    System.out.println("jestemy  w stanie:"+ State + ", Request");
                    System.out.println("Agent o nazwie" + myAgent.getName() + "zainicjowal" +
                            " wymiane");

                    /* Wsylanie oferty wymiany*/
                    ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
                    AID[] waiters = WaiterDirectory.findWaiters(myAgent);

                    msg.setConversationId("Change-Order");

                    for (AID i : waiters) {
                        if (i != myAgent.getAID()) {
                            msg.addReceiver(i);
                        }
                    }

                    /* @TODO check this value */
                    numberOfReceivers = waiters.length - 1;

                    worstOrder = myAgent.findWorstOrder();

                    System.out.println("Agent o nazwie" + myAgent.getName() +" chce wymienic: " +
                    worstOrder.getOrderId());


                    msg.setContentObject(worstOrder);
                    myAgent.send(msg);

                    State = 1;
                    break;

                case (1):
                    /* Czekamy na odpowiedz oferty wymiany */
                    MessageTemplate mt = MessageTemplate.MatchConversationId("Change-Order");

                    //ACLMessage receive = myAgent.blockingReceive(mt, 1500);
                    ACLMessage receive = myAgent.receive(mt);

                   // System.out.println("jestemy  w stanie:"+ State + ", Request");


                    if (receive != null) {


                        System.out.println("otrzymalismy wiadomosc : Request");

                        System.out.println("numerOfReques: " + numberOfReceivers);

                        if (receive.getPerformative() == ACLMessage.ACCEPT_PROPOSAL) {

                            OrderInfo orderinfo = new OrderInfo();
                            orderinfo.order = findBestOrder(
                                    (ArrayList<Order>) receive.getContentObject());
                            orderinfo.agent = receive.getSender();
                            ProposedOrders.add(orderinfo);

                        }

                      //  if (receive.getPerformative() == ACLMessage.REJECT_PROPOSAL) {
                            numberOfReceivers--;
                            if (numberOfReceivers <= 0) {

                                System.out.println("wszyscy agenci odpowiedzili");

                                State = 2;
                            }
                      //  }

                    }

                    break;

                case (2):
                    /* Skompletowalismy wszystkie odpowiedz na oferty,
                     * teraz szukamy najlepszej */

                    ACLMessage inform = new ACLMessage(ACLMessage.INFORM);
                    OrderInfo bestOrder;
                    Order[] reply = new Order[2];

                    System.out.println("jestemy  w stanie:"+ State + ", Request");


                    bestOrder = findBestOrder();
                    inform.addReceiver(bestOrder.agent);
                    reply[0] = bestOrder.order;
                    reply[1] = worstOrder;

                    inform.setContentObject(reply);


                    myAgent.send(inform);

                    /* wymien zamowinie */
                    myAgent.replaceOrder(worstOrder, bestOrder.order);

                    for (OrderInfo i : ProposedOrders) {
                        inform.addReceiver(i.agent);
                        inform.setContent("NOT_OK");
                        myAgent.send(inform);
                    }

                    State = 0;
                    break;

            }
        } catch (Exception e) {
            System.out.println("Saw exception in BehaviorChangeOrderRequest action : " + e);
            e.printStackTrace();
        }
    }
}
/*
    @Override
    public boolean done() {
        if (State == 3) {
            System.out.println("Agent zakonczyl wymiane");
            return true;
        } else {
            return false;
        }
    }

}

*/