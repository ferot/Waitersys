package pl.edu.pw.waitersys;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import pl.edu.pw.waitersys.messages.WaiterPropositionMessage;

import java.io.IOException;

/**
 * Zachowanie w przypadku otrzymania decyzji o (nie)przydzieleniu zamówienia.
 */
public class DecisionBehaviour extends CyclicBehaviour {
    //referencja na agenta adaptującego zachowanie
    WaiterAgent waiter;

    public DecisionBehaviour(Agent a) {
        super(a);
        waiter = (WaiterAgent) a;
    }

    public void action() {
        ACLMessage msg = myAgent.receive();

        if (msg != null) {

            if (msg.getPerformative() == ACLMessage.ACCEPT_PROPOSAL) { //Decyzja o przydzieleniu zamówienia
                    System.out.println("Agent [" + myAgent.getName() + "] otrzymał przydział zamówienia !" );
                    waiter.addCachedOrder();

            } else if (msg.getPerformative() == ACLMessage.REJECT_PROPOSAL) {
                System.out.println("Agent [" + myAgent.getName() + "] otrzymał odmowę zamówienia..." );
                waiter.removeCachedOrder();
            }
        } else {
            // uśpij jesli brak wiadomości
            block();
        }
    }

}
