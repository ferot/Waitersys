package pl.edu.pw.waitersys;

import jade.core.Agent;
import pl.edu.pw.waitersys.directory.WaiterDirectory;
import pl.edu.pw.waitersys.gui.WaiterGUI;

import java.lang.reflect.Array;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


/**
 * Agent reprezentujący interesy kelnera.
 */
public class WaiterAgent extends Agent {

    private ArrayList<Order> orders = new ArrayList<>();
    private int location;
    private float tips;
    private String name;
    private final Lock _mutex = new ReentrantLock(true);

    private Order cachedOrder = null;

    /**
     * Inicjalizuje agenta: rejestruje go w katalogu kelnerów.
     */
    protected void setup() {
        System.out.println("Przyszedł nowy kelner! Identyfikator: " + getAID().getName());
        tips = 0;
        location = 0;

        // Zarejestruj się w katalogu kelnerów.
        WaiterDirectory.announce(this);

        addBehaviour(new SendVoiceBehaviour(this));
        addBehaviour(new DecisionBehaviour(this));

        addBehaviour(new BehaviourChangeOrderResponse(this));
        addBehaviour(new BehaviourChangeOrderRequest(this));

        gui = new WaiterGUI(this, getAID().getLocalName());
        gui.setVisible(true);
    }

    /**
     * Obsługuje śmierć agenta: wyrejestrowuje go z katalogu kelnerów.
     */
    protected void takeDown() {
        System.out.println("Kelner " + getAID().getName() + " opuścił lokal.");

        // Wyrejestruj się z katalogu kelnerów.
        WaiterDirectory.renounce(this);
    }

    public int getOrderCount(){ return orders.size(); }
    public float getTips(){ return tips; }
    public int getLocation() { return location; }

    protected void addCachedOrder() {
        _mutex.lock();
        if (cachedOrder != null) {
            orders.add(cachedOrder);
            System.out.println("Agent " + getName() + " dodaje zamówienie : " + cachedOrder);

            gui.refresh();
        }
        _mutex.unlock();
    }

    protected void setCachedOrder(final Order order){
        _mutex.lock();
        cachedOrder = order;
        _mutex.unlock();
    }

    public void removeCachedOrder(){
            _mutex.lock();
            cachedOrder = null;
            _mutex.unlock();
        }

    /**
     * Oblicza aktualną siłę głosu na żądanie stolika.\
     * Uwzględnia aktualną ilość napiwków i zadań trzymanych na sobie
     *
     *
     * @return wartość siły głosu
     */
    protected double calcVoice() {
        System.out.println("Obliczam siłę głosu...");

        //czynnik +1 dodany by uniknąć dzielenia przez 0
        int taskCountFactor = 1/(getOrderCount() + 1);
        float tipsFactor = 1/(getTips() + 1);

        double voiceVolume = (taskCountFactor+ tipsFactor)*0.5;

        return voiceVolume;
    }

    public Order findWorstOrder()
    {
        Order worstOrder = new Order();
        int worstDiff = 0;

        for (Order o:orders) {
            if (o != null) {
                int tmp = Math.abs(this.location - o.getLocation());
                if (tmp > worstDiff) {
                    worstDiff = tmp;
                    worstOrder = o;
                }
            }
        }

        return worstOrder;
    }

    public ArrayList<Order> findWorseOrder(Order compareOrder)
    {
        ArrayList<Order> WorseOrders = new ArrayList<>();
        int compareLocationDiff = Math.abs(compareOrder.getLocation() - this.location);

        for (Order o:orders) {
            if (o != null) {
                if (compareLocationDiff > Math.abs(this.location - o.getLocation())) {
                    WorseOrders.add(o);
                }
            }

        }

        return WorseOrders;
    }

    public void replaceOrder(Order Old, Order New) {
        /* @TODO implement this */
        Iterator<Order> iter = orders.iterator();


        List<Order> toRemove = new ArrayList<>();
        for (Order o : orders) {
            if (o != null) {
                if (o.getOrderId().equals(Old.getOrderId())) {
                    toRemove.add(o);
                }
            }
        }
        orders.removeAll(toRemove);
        orders.add(New);




/*

        while (iter.hasNext()) {

            Order o = iter.next();

            if (o.getOrderId().equals(Old.getOrderId())) {
                iter.remove();
                orders.add(New);
            }

        }
*/

/*
        for (Order o:orders) {
            if (o.getOrderId().equals(Old.getOrderId())) {
                orders.remove(o);
                orders.add(New);
            }

        }
*/
    }

    /**
     * Zwraca listę zamówień przypisanych do kelnera.
     */
    public List<Order> getOrders() {
        return orders;
    }

    /**
     * Usuwa zamówienie o danym identyfikatorze z listy zamówień kelnera.
     *
     * @param orderId Unikatowy identyfikator zamówienia
     */
    public void markOrderDone(UUID orderId) {
        orders.removeIf(order -> order != null && order.getId() == orderId);
    }

    public void locationActualization()
    {
        if (!orders.isEmpty()) {
            this.location = orders.get(0).getLocation();

        }
    }

    /**
     * GUI związane z agentem.
     */
    private WaiterGUI gui;
}
