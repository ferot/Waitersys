package pl.edu.pw.waitersys;

import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import pl.edu.pw.waitersys.Order;

import jade.domain.FIPAException;
import java.util.ArrayList;

public class BehaviourChangeOrderResponse extends CyclicBehaviour {
    private WaiterAgent myAgent; // Agent na rzecz którego wykonywane jest zachowanie
    private int State = 0;
    private Order orderRecived;
    private ArrayList<Order> worseOrders = new ArrayList<>();

    public BehaviourChangeOrderResponse(WaiterAgent waiterAgent) {
        myAgent = waiterAgent;
    }


    @Override
    public void action() {
        try {
            // Uaktualnienie wartosci lokalizacji kelnera
            myAgent.locationActualization();


            switch (State) {
                case (0):
                    /* Oczekiwanie na oferte wymiany */
                    MessageTemplate mt = MessageTemplate.MatchConversationId("Change-Order");

                    ACLMessage msg = myAgent.receive(mt);


                    System.out.println("jestemy  w stanie:"+ State + ", Response"+
                            " agent: " + myAgent.getName());

                    if (msg != null && msg.getPerformative() == ACLMessage.REQUEST) {

                        ACLMessage reply = msg.createReply();

                        reply.setConversationId("Change-Order");

                        if ( msg.getContentObject() instanceof Order) {

                            orderRecived = (Order) msg.getContentObject();


                            worseOrders = myAgent.findWorseOrder(orderRecived);

                            System.out.println("Agent o nazwie" + myAgent.getName() +
                                    " dostal propzycje: " + orderRecived.getOrderId() + "od agenta"
                            + msg.getSender().getName()) ;


                            if (!worseOrders.isEmpty()) {
                                reply.setPerformative(ACLMessage.ACCEPT_PROPOSAL);
                                reply.setContentObject(worseOrders);

                                System.out.println("Agent o nazwie" + myAgent.getName() +
                                "zgodzil sie na propzycje " +  orderRecived.getOrderId() + "od agenta"
                                        + msg.getSender().getName());

                                myAgent.send(reply);
                                State = 1;

                            } else {
                                reply.setPerformative(ACLMessage.REJECT_PROPOSAL);
                                myAgent.send(reply);

                                System.out.println("Agent o nazwie" + myAgent.getName() +
                                        "NIE ZGODZIL SIE na:  " +  orderRecived.getOrderId() + "od agenta"
                                        + msg.getSender().getName());

                                State = 0;

                            }
                        }

                    } else {
                        /* tu chyba troche nie tak*/
                        block();
                    }

                    break;

                case (1):
                    /* czekamy na potwierdzenie wymiany*/
                    MessageTemplate t = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
                    System.out.println("jestemy  w stanie:"+ State + ", Response"
                    + " agent: " + myAgent.getName());

                    ACLMessage backMsg = myAgent.blockingReceive(t, 1000);
                    //ACLMessage backMsg = myAgent.receive(t);

                    if (backMsg != null) {
                        if (!backMsg.getContent().equals("NOT_OK")) {
                            Order[] response = (Order[]) backMsg.getContentObject();
                            myAgent.replaceOrder(response[1], response[0]);

                            System.out.println("Agent o nazwie: " + myAgent.getName() +
                                    " wymienil zamowienie: " + response[1].getOrderId() +
                                    " na zamowinie: " + response[0].getOrderId() +
                                    "z agentem o nazwie: " + backMsg.getSender().getName());



                        }


                    }
                    State = 0;
                    //else {
                       // block();
                    //}

            }
        } catch (Exception e) {
            System.out.println( "Saw exception in BehaviorChangeOrderResponse action : " + e );
            e.printStackTrace();
        }
    }
}