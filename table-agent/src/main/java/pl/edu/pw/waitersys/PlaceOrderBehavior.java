package pl.edu.pw.waitersys;

import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;
import pl.edu.pw.waitersys.directory.WaiterDirectory;
import pl.edu.pw.waitersys.messages.OrderMessage;
import pl.edu.pw.waitersys.messages.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;
import java.io.Serializable;

/**
 * Jednorazowe zachowanie agenta stolika polegające na rozesłaniu kelnerom wybranego zamówienia.
 */
public class PlaceOrderBehavior extends OneShotBehaviour {
    /**
     * Tworzy zachowanie.
     *
     * @param order Zamówienie wybrane przez gości przy stoliku.
     */
    PlaceOrderBehavior(final Order order, final int location) {
        this.order = order;
        this.location = location;

        // Przypisanie lokalizacji stolika do zamowienia

        this.order.setLocation(this.location);
    }

    /**
     * Wykonuje to zachowanie: wyszukuje dostępnych kelnerów i jeżeli jacyś są w lokalu,
     * informuje ich o wybranym zamówieniu za pośrednictwem wiadomości typu [[OrderMessage]].
     *
     * W przypadku błędu komunikacji, poddaje się bezsilnie.
     */
    public void action() {
        // 1. Wyszukaj zarejestrowanych kelnerów.
        final AID[] waiters = WaiterDirectory.findWaiters(myAgent);

        // 2. Jeżeli nie ma żadnych kelnerów, oburz się i zamachaj ramionami.
        if (waiters.length == 0) {
            System.err.println("Nie znaleziono żadnych kelnerów!");
            return;
        }
        String cid = UUID.randomUUID().toString();

        // 3. Roześlij do wszystkich swoje zamówienie. Jeżeli się nie uda, oburz się.
        try {
            OrderMessage msg = new OrderMessage(order, waiters);
            msg.setConversationId(cid);
            myAgent.send(msg);
        } catch (IOException exc) {
            System.err.println("Stolik " + myAgent.getName() + " stracił łączność!");
        }

        MessageTemplate mt = MessageTemplate.MatchConversationId(cid);
        ArrayList<WaiterProposition> propositions = new ArrayList<>();
        ArrayList<AID> from = new ArrayList<>();

        // 4. Odbierz wiadomości, czekając max 100ms
        try {
            ACLMessage msg;
            while(true) {
                msg = myAgent.blockingReceive(mt, 100);
                if(msg == null) break;
                propositions.add((WaiterProposition)msg.getContentObject());
                from.add(msg.getSender());
            }
        } catch (UnreadableException exc) {
            System.err.println("Stolik " + myAgent.getName() + " nie umie deserializować!");
        }
        if(from.size() == 0) {
            System.err.println("Stolik " + myAgent.getName() + " nie dostał żadnych propozycji!");
            return;
        }

        // 5. wybór najlepszego stolika
        int bestIndex = 0;
        double bestValue = Double.POSITIVE_INFINITY;
        for(int i = 0; i < propositions.size(); ++i)
        {
            WaiterProposition prop = propositions.get(i);

            double str = prop.getVoiceStrength();
            int dist = Math.abs(location - prop.getLocation());
            double val = str/dist;
            if(val < bestValue) {
                bestValue = val;
                bestIndex = i;
            }
        }

        // 6. wysłanie ACCEPT_PROPOSAL
        try {

            OrderAssignmentMessage msg = new OrderAssignmentMessage(new OrderAcceptance(), from.get(bestIndex));
            msg.setConversationId(cid);

            myAgent.send(msg);
        } catch (IOException exc) {
            System.err.println("Stolik " + myAgent.getName() + " stracił łączność!");
        }

        from.remove(bestIndex);

        // 6. wysłanie REJECT_PROPOSAL
        try {
            OrderAssignmentMessage msg = new OrderAssignmentMessage(new OrderRefusal(), from.toArray(new AID[from.size()]));

            msg.setConversationId(cid);
            myAgent.send(msg);
        } catch (IOException exc) {
            System.err.println("Stolik " + myAgent.getName() + " stracił łączność!");
        }

    }

    private final Order order;
    private final int location;
}
