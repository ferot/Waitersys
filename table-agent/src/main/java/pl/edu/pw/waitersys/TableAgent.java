package pl.edu.pw.waitersys;

import jade.core.Agent;
import pl.edu.pw.waitersys.gui.TableGUI;

/**
 * Agent zarządzający pojedynczym stolikiem w restauracji.
 *
 * Udostępnia proste GUI dla gości, za pomocą którego mogą oni wybrać swoje zamówienie
 * i poinformować o nim znajdujących się w lokalu kelnerów.
 */
public class TableAgent extends Agent {
    /**
     * Obsługuje wybór przez gości zamówienia z poziomu GUI.
     *
     * Dodaje do kolejki zachowanie polegające na rozejrzeniu się za kelnerami i wykrzyczeniu
     * wybranego zamówienia. Po wykonaniu zachowania, agent usypia.
     *
     * TODO Nie należy zakładać, że zamówienie ma jedną pozycję!
     *
     * @param order Zamówienie wybrane przez gości.
     */
    public void handleOrder(final Order order) {
        System.out.println(getName() + ": Zamawiam " + order);

        addBehaviour(new PlaceOrderBehavior(order, Integer.parseInt(agentName.substring(5))));
    }

    /**
     * Inicjalizuje agenta: uruchamia GUI i usypia.
     */
    protected void setup() {
        agentName = getAID().getLocalName();

        System.out.println("Utworzono nowy stolik! Identyfikator: " + agentName);

        new TableGUI(this, agentName).setVisible(true);
    }

    /**
     * Obsługuje śmierć agenta.
     */
    protected void takeDown() {
        System.out.println("Stolik " + getAID().getName() + " opuścił lokal.");
    }

    private String agentName;
}
