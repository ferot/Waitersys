package pl.edu.pw.waitersys.gui;

import pl.edu.pw.waitersys.Food;
import pl.edu.pw.waitersys.Order;
import pl.edu.pw.waitersys.TableAgent;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Arrays;

/**
 * Proste GUI związane z wybranym agentem stolikowym, pozwalające na wybór zamówienia.
 *
 * Składa się z dwóch paneli:
 *   - Menu z klikalnymi pozycjami,
 *   - Informacji o obecnie tworzonym zamówieniu oraz poprzednich zamówieniach.
 */
public class TableGUI extends JFrame {
    /**
     * Konstruuje GUI. Okno jest nierozerwalnie związane z podanym agentem.
     *
     * <p>
     * Okno nie jest wyświetlane bezpośrednio po skonstruowaniu. Aby było widoczne,
     * należy jawnie wywołać metodę <pre>setVisible(true)</pre>!.
     * </p>
     *
     * @param owner     Agent będący właścicielem GUI.
     * @param tableName Nazwa stolika wyświetlana jako tytuł okna.
     */
    public TableGUI(TableAgent owner, final String tableName) {
        super(tableName);

        this.owner = owner;
        this.tableName = tableName;
        this.activeOrder = new Order(tableName);
        this.activeOrderInfo = new JLabel(activeOrder.toHTML());

        sizeIt();

        renderMenu();
        renderInfo();

        addWindowListener(new CloseHandler());
    }

    /**
     * Wylicza właściwe wymiary okna i ustawia layout.
     */
    private void sizeIt() {
        setSize(
            2 * POSITION_WIDTH + 50,       // Połowa na menu, połowa na informacje, miejsce na scrollbar.
            menu.length * POSITION_HEIGHT // Długość pokrywająca się z długością menu.
        );

        setLayout(new GridLayout(0, 2));
    }

    /**
     * Dodaje do okna przyciski odpowiadające dostępnym potrawom.
     */
    private void renderMenu() {
        final Panel panel = new Panel(new GridLayout(0, 1));

        Arrays.stream(menu)
            .map(food -> {
                final Button button = new Button(
                        String.format("%s: %.2f PLN", food.readableName(), food.getPrice())
                );
                button.addActionListener(new FoodClickHandler(food));
                return button;
            })
            .forEach(panel::add);

        add(panel);
    }

    /**
     * Dodaje do okna panel z przyciskami zamawiania i odrzucania zamówienia oraz
     * informacjami o obecnym i poprzednich zamówieniach.
     */
    private void renderInfo() {
        final ScrollPane scrollPane = new ScrollPane(ScrollPane.SCROLLBARS_AS_NEEDED);
        final Panel panel = new Panel(new BorderLayout());

        // Przyciski zamawiania i odrzucania obecnego zamoówienia:
        final Panel buttonsPanel = new Panel();
        buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.Y_AXIS));

        final Button orderButton = new Button("ZAMÓW");
        orderButton.addActionListener(new PlaceOrderHandler());
        final Button clearButton = new Button("WYCZYŚĆ");
        clearButton.addActionListener(new ClearOrderHandler());

        buttonsPanel.add(orderButton, BorderLayout.PAGE_START);
        buttonsPanel.add(clearButton, BorderLayout.PAGE_START);
        panel.add(buttonsPanel, BorderLayout.PAGE_START);

        // Informacje o obecnym zamówieniu:
        final Panel infoPanel = new Panel();
        infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.Y_AXIS));

        infoPanel.add(new JLabel("<html><h2>Bieżące zamówienie</h2></html>"));
        infoPanel.add(activeOrderInfo);

        // Informacje o poprzednich zamówieniach:
        infoPanel.add(new JLabel("<html><h2>Poprzednie zamówienia</h2></html>"));
        previousOrdersInfo.setLayout(new BoxLayout(previousOrdersInfo, BoxLayout.Y_AXIS));
        infoPanel.add(previousOrdersInfo);

        panel.add(infoPanel, BorderLayout.CENTER);
        scrollPane.add(panel);
        add(scrollPane);
    }

    /**
     * Obsługuje zdarzenia lifecycle'owe okna.
     */
    private class CloseHandler extends WindowAdapter {
        /**
         * Obsługuje prośbę o zamknięcie okna (na przykład po kliknięciu X).
         * Zabija agenta-właściciela, a później samo okno.
         */
        public void windowClosing(WindowEvent event) {
            owner.doDelete();
            dispose();
        }
    }

    /**
     * Obsługuje kliknięcia w przycisk związany z wybraną potrawą.
     * Powiadamia agenta o wybranym zamówieniu.
     */
    private class FoodClickHandler implements ActionListener {
        FoodClickHandler(final Food handledFood) {
            this.handledFood = handledFood;
        }

        public void actionPerformed(ActionEvent e) {
            activeOrder.addPosition(handledFood);
            activeOrderInfo.setText(activeOrder.toHTML());
            repaint();
        }

        private final Food handledFood;
    }

    /**
     * Obsługuje kliknięcia w przycisk składania zamówienia.
     */
    private class PlaceOrderHandler implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (!activeOrder.isEmpty()) {
                owner.handleOrder(activeOrder);

                // Dodaj informację do panelu przeszłych zamówień:
                previousOrdersInfo.add(new JLabel(activeOrder.toHTML()));

                activeOrder = new Order(tableName);
                activeOrderInfo.setText(activeOrder.toHTML());

                repaint();
            }
        }
    }

    /**
     * Obsługuje kliknięcia w przycisk czyszczenia obecnego zamówienia.
     */
    private class ClearOrderHandler implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            activeOrder = new Order(tableName);
            activeOrderInfo.setText(activeOrder.toHTML());
            repaint();
        }
    }

    /**
     * Szerokość przycisku z potrawą.
     */
    private static final int POSITION_WIDTH = 250;

    /**
     * Wysokość przycisku z potrawą.
     */
    private static final int POSITION_HEIGHT = 50;

    /**
     * Pozycje dostępne w menu.
     */
    private static final Food[] menu = Food.values();

    /**
     * Agent będący właścicielem GUI.
     */
    private final TableAgent owner;

    /**
     * Nazwa stolika, do którego należy GUI.
     */
    private final String tableName;

    /**
     * Obecnie przygotowywane zamówienie.
     */
    private Order activeOrder;

    /**
     * Label z informacją o obecnym zamówieniu.
     */
    private final JLabel activeOrderInfo;

    /**
     * Panel z informacjami o poprzednich zamówieniach.
     */
    private final JPanel previousOrdersInfo = new JPanel();
}
