package pl.edu.pw.waitersys;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Reprezentuje zamówienie.
 *
 * Zamówienie rozumiemy po prostu jako listę potraw.
 */
public class Order implements Serializable {
    /**
     * Konstruuje zamówienie zawierające wybrane potrawy. Może być puste.
     *
     * @param table Nazwa zamawiającego stolika.
     * @param foods Potrawy wchodzące w skład zamówienia.
     */

    public Order(final String table, final Food... foods) {
        this.positions = new ArrayList<>(Arrays.asList(foods));
        this.table = table;
        this.id = UUID.randomUUID();
        orderId = UUID.randomUUID().toString();
    }

    public Order() {
        this.positions = new ArrayList<>();
        this.table = new String();
        this.id = UUID.randomUUID();
        this.orderId = UUID.randomUUID().toString();
    }

    public void setLocation(int location) { this.location = location;}
    public int getLocation() {return this.location;}
    public String getOrderId() {return this.orderId;}
    private final UUID id;

    /**
     * Dodaje nową potrawę do zamówienia.
     *
     * Potrawa może się pokrywać z którąś z istniejących pozycji - wówczas zostanie
     * dostarczona wielokrotnie.
     *
     * @param meal Potrawa dopisywana do zamówienia.
     */
    public void addPosition(final Food meal) {
        positions.add(meal);
    }

    /**
     * Zwraca listę pozycji zamówienia.
     */
    public List<Food> getPositions() {
        return positions;
    }

    /**
     * Zwraca informację o tym czy zamówienie jest puste (nie zawiera pozycji).
     */
    public boolean isEmpty() {
        return positions.isEmpty();
    }

    /**
     * Usuwa pojedyncze wystąpienie podanej potrawy z zamówienia.
     * Jeżeli zamówienie nie zawiera wybranej potrawy, pozostaje niezmienione.
     */
    public void removePosition(final Food meal) {
        positions.remove(meal);
    }

    private final ArrayList<Food> positions;
    private final String orderId;
    private int location;

    /**
     * Zwraca nazwę stolika składającego zamówienie.
     */
    public String getTable() {
        return table;
    }

    /**
     * Zwraca (losowy) identyfikator zamówienia.
     */
    public UUID getId() {
        return id;
    }

    /**
     * Zwraca łączną wartość zamówienia.
     */
    public double getTotalPrice() {
        return positions.stream()
                .mapToDouble(Food::getPrice)
                .sum();
    }

    /**
     * Zwraca skróconą reprezentację tekstową zamówienia.
     */
    @Override
    public String toString() {
        final String combined =
            positions.stream()
                .map    (Food::toString)
                .collect(Collectors.joining(", "));

        return "Order(to: " + table + ", positions: " + combined + ")";
    }

    /**
     * Zwraca obszerną reprezentację tekstową zamówienia jako HTML.
     */
    public String toHTML() {
        return toHTML(false);
    }

    /**
     * Zwraca obszerną reprezentację tekstową zamówienia jako HTML, z uwzględnieniem nazwy stolika.
     */
    public String toHTMLWithTableName() {
        return toHTML(true);
    }

    /**
     * Zwraca obszerną reprezentację tekstową zamówienia jako HTML.
     *
     * @param withTableName Czy nazwa stolika powinna być dołączona do wynikowego HTML-a?
     */
    private String toHTML(final boolean withTableName) {
        final String tableStr =
            withTableName?
                "<h3>Stolik: " + table + "</h3>" :
                "";

        final String elements =
            positions.stream()
                .map(food -> String.format(
                    "<li>%s (%.2f)</li>", food.readableName(), food.getPrice()
                ))
                .collect(Collectors.joining());

        final String priceStr =
            String.format("%.2f", getTotalPrice());

        return
            "<html>" +
                "<h3>Pozycje:</h3>" +
                tableStr +
                "<ul>" +
                    elements +
                "</ul>" +
                "<h3>Łączna cena: " + priceStr + "</h3>" +
            "</html>";
    }

    private final String table;
}
