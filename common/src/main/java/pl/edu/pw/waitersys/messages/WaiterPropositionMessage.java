package pl.edu.pw.waitersys.messages;

import jade.core.AID;
import jade.lang.acl.ACLMessage;
import pl.edu.pw.waitersys.WaiterProposition;

import java.io.IOException;

/**
 * Odpowiedź na [[OrderMessage]], kelner wysyła ją określając swoją propozycję wykonania usługi (obsługi stolika).
 *
 * Performatywa: PROPOSE
 */
public class WaiterPropositionMessage extends ACLMessage {
    public WaiterPropositionMessage(final WaiterProposition waiterProposition, final AID table) throws IOException {
        super(ACLMessage.PROPOSE);
        this.addReceiver(table);

        try {
            setContentObject(waiterProposition);
        } catch (IOException exc) {
            System.err.println("Nie udało się przygotować wiadomości z zamówieniem!");
            throw exc;
        }
    }
}
