package pl.edu.pw.waitersys;

import java.io.Serializable;

/**
 * Odpowiedź na zamówienie przez kelnera. Zawiera siłę głosu i położenie kelnera.
 */
public class WaiterProposition implements Serializable {

    /** Siła głosu kelnera */
    final private double voiceStrength;

    /** Pozycja kelnera (numer najbliższego stolika) */
    final private int location;

    public WaiterProposition(double voiceStrength, int location) {
        this.voiceStrength = voiceStrength;
        this.location = location;
    }

    public double getVoiceStrength() {
        return voiceStrength;
    }

    public int getLocation() {
        return location;
    }
}
