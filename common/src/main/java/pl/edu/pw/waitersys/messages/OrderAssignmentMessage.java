package pl.edu.pw.waitersys.messages;

import jade.core.AID;
import jade.lang.acl.ACLMessage;
import pl.edu.pw.waitersys.OrderAcceptance;
import pl.edu.pw.waitersys.OrderRefusal;

import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;

/**
 * Odpowiedź na [[WaiterProposition]], stolik wysyła ją do kelnerów informując o wybranym wykonawcy.
 *
 * Performatywa: ACCEPT_PROPOSAL, REJECT_PROPOSAL
 */
public class OrderAssignmentMessage extends ACLMessage {

    private OrderAssignmentMessage(final int messageType, final Serializable body, final AID[] waiters) throws IOException {
        super(messageType);

        Arrays.stream(waiters).forEach(this::addReceiver);

        try {
            setContentObject(body);
        } catch (IOException exc) {
            System.err.println("Nie udało się przygotować wiadomości z zamówieniem!");
            throw exc;
        }
    }

    //tylko 1 propozycja moze byc zaakceptowana, tablica tu nie ma sensu
    public OrderAssignmentMessage(final OrderAcceptance orderAssignment, final AID waiter) throws IOException {
        super(ACLMessage.ACCEPT_PROPOSAL);
        addReceiver(waiter);
    }

    public OrderAssignmentMessage(final OrderRefusal orderAssignment, final AID[] waiters) throws IOException {
        super(ACLMessage.REJECT_PROPOSAL);
        Arrays.stream(waiters).forEach(this::addReceiver);
    }
}
