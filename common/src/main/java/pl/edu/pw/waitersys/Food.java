package pl.edu.pw.waitersys;

/**
 * Definiuje potrawy dostępne w lokalu.
 *
 * Każda potrawa posiada czytelną dla człowieka nazwę.
 */
public enum Food {
    SPAGHETTI_BOLOGNESE ("Spaghetti Bolognese", 15.90),
    BEEF_BURGER         ("Burger z wołowiny", 21.90),
    PAD_THAI            ("Pad thai z kurczakiem", 19.90),
    KEBAB               ("Kebab z baraniny", 14.90),
    FISH                ("Ryba smażona", 13.90),
    SCHNITZEL           ("Kotlet schabowy", 12.90),
    POTATOES            ("Ziemniaki", 4.00),
    RICE                ("Ryż", 4.00),
    COMPOT              ("Kompot", 2.00),
    JUICE               ("Sok", 3.00),
    TEA                 ("Herbata", 3.00),
    COFFEE              ("Kawa", 4.00),
    BEER                ("Piwo", 6.00),
    DESSERT             ("Ciasto z lodami", 7.00);

    /**
     * Zwraca czytelną nazwę potrawy.
     */
    public String readableName() {
        return rName;
    }

    public double getPrice(){return price; }

    Food(final String readableName, double price) {
        this.rName = readableName;
        this.price = price;
    }

    private final String rName;
    private double price;
}
