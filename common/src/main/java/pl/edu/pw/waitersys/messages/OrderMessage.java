package pl.edu.pw.waitersys.messages;

import jade.core.AID;
import jade.lang.acl.ACLMessage;
import pl.edu.pw.waitersys.Order;

import java.io.IOException;
import java.util.Arrays;

/**
 * Wiadomość przesyłana przez agenta zarządzającego stolikiem do wybranych kelnerów
 * w momencie, gdy goście przy stoliku sformułują swoje zamówienie.
 *
 * Wykorzystuje performatywę REQUEST. Treścią wiadomości jest zserializowany obiekt zamówienia.
 */
public class OrderMessage extends ACLMessage {
    public OrderMessage(final Order order, final AID[] waiters) throws IOException {
        super(ACLMessage.REQUEST);

        Arrays.stream(waiters).forEach(this::addReceiver);

        try {
            setContentObject(order);
        } catch (IOException exc) {
            System.err.println("Nie udało się przygotować wiadomości z zamówieniem!");
            throw exc;
        }
    }
}
