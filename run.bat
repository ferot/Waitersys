set LAUNCHED_AGENTS="table1:pl.edu.pw.waitersys.TableAgent; table2:pl.edu.pw.waitersys.TableAgent; table3:pl.edu.pw.waitersys.TableAgent; waiter1:pl.edu.pw.waitersys.WaiterAgent; waiter2:pl.edu.pw.waitersys.WaiterAgent"

set /p CLASSPATH= < classpath.txt
mkdir run && cd run

java -cp %CLASSPATH% jade.Boot -agents %LAUNCHED_AGENTS%
